/* The Linux Kernel Module Programming Guide
 * =========================================
 *
 * References
 * ----------
 *
 *  - <http://tldp.org/LDP/lkmpg/2.6/html/lkmpg.html>
 *  - <http://tldp.org/LDP/lkmpg/2.6/html/lkmpg.html#AEN119>
 *
 *
 * Chapter
 * -------
 *
 * ### 2.1 Hello, World (part 1): The Simplest Module
 *
 *  Example 2-1. hello-1.c
 *
 *
 * Compiling and Loading
 * ---------------------
 *
 *	$ make
 *	$ sudo insmod ./hello-1.ko
 *	$ sudo rmmod hello-1
 */

#include <linux/module.h> /* Needed by all modules */
#include <linux/kernel.h> /* Needed for KERN_INFO  */


/** Init Module.
 *
 * @retval 0  init module was successful
 * @retval !0 init module failed
 */
int init_module(void)
{
	printk(KERN_INFO "Hello Linux\n");
	return 0;
}


/** Cleanup Module. */
void cleanup_module(void)
{
	printk(KERN_INFO "Goodbye Linux\n");
}

