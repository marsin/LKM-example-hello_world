/* The Linux Kernel Module Programming Guide
 * =========================================
 *
 * References
 * ----------
 *
 *  - <http://tldp.org/LDP/lkmpg/2.6/html/lkmpg.html>
 *  - <http://tldp.org/LDP/lkmpg/2.6/html/lkmpg.html#AEN119>
 *
 *
 * Chapter
 * -------
 *
 * ### 2.4 Hello, World (part 4)
 *
 *  Example 2-5. hello-3.c
 *
 *  Illustrating the __init, __initdata and __exit macros.
 *
 *
 * Compiling and Loading
 * ---------------------
 *
 *	$ make
 *	$ sudo insmod ./hello-3.ko
 *	$ sudo rmmod hello-3
 */

#include <linux/module.h> /* Needed by all modules */
#include <linux/kernel.h> /* Needed for KERN_INFO  */
#include <linux/init.h>   /* Needed for the macros */


static int Hello_3_data __initdata = 3;

static int __init hello_3_init(void)
{
	printk(KERN_INFO "Hello, world %d\n", Hello_3_data);
	return 0;
}


static void __exit hello_3_exit(void)
{
	printk(KERN_INFO "Goodbye, world %d\n", Hello_3_data);
}


module_init(hello_3_init);
module_exit(hello_3_exit);

