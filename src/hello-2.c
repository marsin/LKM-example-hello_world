/* The Linux Kernel Module Programming Guide
 * =========================================
 *
 * References
 * ----------
 *
 *  - <http://tldp.org/LDP/lkmpg/2.6/html/lkmpg.html>
 *  - <http://tldp.org/LDP/lkmpg/2.6/html/lkmpg.html#AEN119>
 *
 *
 * Chapter
 * -------
 *
 * ### 2.3 Hello, World (part 2)
 *
 *  Example 2-3. hello-2.c
 *
 *
 * Compiling and Loading
 * ---------------------
 *
 *	$ make
 *	$ sudo insmod ./hello-2.ko
 *	$ sudo rmmod hello-2
 */

#include <linux/module.h> /* Needed by all modules */
#include <linux/kernel.h> /* Needed for KERN_INFO  */


static int __init hello_2_init(void)
{
	printk(KERN_INFO "Hello, world 2\n");
	return 0;
}


static void __exit hello_2_exit(void)
{
	printk(KERN_INFO "Goodbye, world 2\n");
}


module_init(hello_2_init);
module_exit(hello_2_exit);

