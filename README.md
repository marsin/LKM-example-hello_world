Linux Kernel Module - Example - Hello World
===========================================

 Examples for basic Linux kernel modules.


References
----------

 - <http://tldp.org/LDP/lkmpg/2.6/html/lkmpg.html>
 - <http://tldp.org/LDP/lkmpg/2.6/html/lkmpg.html#AEN119>


Chapter
-------

### 2.1 Hello, World (part 1): The Simplest Module

 Example 2-1. hello-1.c


### 2.3 Hello, World (part 2)

 Example 2-3. hello-2.c


### 2.4 Hello, World (part 4)

 Example 2-5. hello-3.c

 Illustrating the `__init`, `__initdata` and `__exit` macros.


Compiling and Loading
---------------------

```sh
$ make
$
$ sudo insmod ./hello-1.ko
$ sudo insmod ./hello-2.ko
$ sudo insmod ./hello-3.ko
$
$ lsmod | grep hello
$
$ sudo rmmod hello-1
$ sudo rmmod hello-2
$ sudo rmmod hello-3
$
$ make clean
```

